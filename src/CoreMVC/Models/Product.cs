﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMVC.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty("_id")]
        public Guid Id { get; set; }
        [JsonProperty("name")]
        public string  Name{ get; set; }
        [JsonProperty("mark")]
        public string Mark { get; set; }

    }
}
